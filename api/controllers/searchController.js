const Fuse = require('fuse.js');
const mongoose = require('mongoose');
var Events = mongoose.model('events');
const fs = require('fs');

exports.searchEvents = function(req, res){
    var eventName = req.body.eventName;
    var eventLocation = req.body.eventLocation;
    // var fromDate = 0;
    // var toDate = 999999999999;
    // console.log(String(req.body.fromDate));
    // if(req.body.fromDate != ""){

    //     var tmp = req.body.fromDate.split('-');
    //     fromDate+= parseInt(tmp[0])*100000000;
    //     fromDate+= parseInt(tmp[1])*1000000;
    //     fromDate+= parseInt(tmp[2])*10000;
    // }

    // if(req.body.toDate != ""){
    //     toDate=0;
    //     var tmp= req.body.toDate.split("-");
    //     toDate+= parseInt(tmp[0])*100000000;
    //     toDate+= parseInt(tmp[1])*1000000;
    //     toDate+= parseInt(tmp[2])*10000;
    // }

    // var tmp = req.body.toDate;
    // console.log("eventName: ",eventName);
    // console.log("eventLocation: ",eventLocation);
    // console.log("Date Range: ",fromDate," to ",toDate);

    Events.find({}).then(searchData=>{
        var options = {
            caseSensitive : false,
            keys : ['EventName', 'Organiser', 'Venue', 'Time', 'Details']
        };
        var fuse = new Fuse(searchData, options);
        if(eventLocation == ""){
            searchData = fuse.search(eventName);
            res.json(searchData);
        }
        if(eventName == ""){
            searchData = fuse.search(eventLocation);
            res.json(searchData);
        }
        else{
            searchData = fuse.search(eventName, eventLocation);
            res.json(searchData);
        }
    }, err => console.log(err)
    );
};
