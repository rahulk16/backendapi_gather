const mongoose = require('mongoose');
var Schema = mongoose.Schema;
var gatherSchema = new Schema({
    Venue : String,
    number : Number,
    Organiser : String,
    EventName : String,
    Details : String,
    Time : String,
    MapUrl : String,
    Tags : String,
    PosterUrl : String,
});

module.exports = mongoose.model('events',gatherSchema);