const mongoose = require('mongoose');
var Users = require('../models/gatherUser');
var Events = mongoose.model('events');
var Algorithmia = require("algorithmia");
const JWT = require('jsonwebtoken');
const {JWT_SECRET} = require('../configuration');

signToken = user => {
	return JWT.sign({
		iss: 'Gather',
		sub: user.id,
		iat: new Date().getTime(),
		exp: new Date().setDate(new Date().getDate() + 1)
	},JWT_SECRET);
}

// exports.signUp = async function(req,res){
// 	const {email, password} = req.value.body;

// 	//check if user alredy exists
// 	const foundUser = await Users.findOne({email});
// 	if(foundUser){
// 		return res.status(403).json({error: 'Email is already in use'});
// 	}

// 	//create new user
// 	const newUser = new Users({email, password});
// 	await newUser.save();

// 	//generate the token
// 	const token = signToken(newUser);

// 	//respond with token
// 	res.status(200).json({token});
// };

exports.signUp = async (req, res, next) => {
    const { email, password } = req.value.body;

    // Check if there is a user with the same email
    const foundUser = await Users.findOne({ "local.email": email });
    if (foundUser) { 
      return res.status(403).json({ error: 'Email is already in use'});
    }

    // Create a new user
    const newUser = new Users({ 
      method: 'local',
      local: {
        email: email, 
        password: password
      }
    });

    await newUser.save();

    // Generate the token
    const token = signToken(newUser);
    // Respond with token
    res.status(200).json({ token });
  };

exports.signIn = async function(req,res){
	//generate token
	console.log(req.user);
	const token = signToken(req.user);
	res.status(200).json({token});
	console.log('Successful login');
};

exports.googleOAuth = async (req, res, next) => {
    // Generate token
    const token = signToken(req.user);
    res.status(200).json({ token });
};

//CRUD Operations on events

function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
}

exports.creatEvents = async function(req, res){
	// res.json({ secret: "resource"});
	// Validate request
    if(!req.body.EventName) {
        return res.status(400).send({
            message: "EventName can not be empty"
        });
    }

    var tagInput = req.body.EventName + " " + req.body.Venue + " " + req.body.Details;
    var tagOutput;
	 Algorithmia.client("simz9BDkWduXmr76RbH/f1EZa6c1")
	  .algo("shashankgutha/TagsorTopicsGenerator/0.2.2?") // timeout is optional
	  .pipe(tagInput)
	  .then(function(response) {
	    console.log(response.get());
	    tagOutput = response.get();
	  });

	  await sleep(7000);

    const event = new Events({
	    Venue : req.body.Venue,
	    number : req.body.number,
	    Organiser : req.body.Organiser,
	    EventName : req.body.EventName,
	    Details : req.body.Details,
	    Time : req.body.Time,
	    MapUrl : req.body.MapUrl,
	    Tags : JSON.stringify(tagOutput),
	    PosterUrl : req.body.PosterUrl,
    });

    await event.save().then(data => {
    	res.send(data);
    	console.log('New Event created:',event.EventName);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the event."
        });
    });
};

exports.getAll = async function(req, res){
	Events.find().then( events => {
		res.send(events);
		console.log("All available events returned...");
	}).catch(er => {
		res.status(500).send({
			message: err.message || "Some error occurred while creating the event."
		});
	});
};

exports.findOneEvent = async function(req, res){
	Events.findById(req.params.eventId).then(event => {
		if(!event){
			return res.status(404).send({
				message: "Event not found with Id: " + req.params.eventId
			});
		}
		res.send(event);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Event not found with id " + req.params.eventId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.eventId
        });
	});
};

exports.updateEventById = (req, res) => {
    // Validate Request
    if(!req.body.EventName) {
        return res.status(400).send({
            message: "Event Name can not be empty"
        });
    }

    // Find note and update it with the request body
    Events.findByIdAndUpdate(req.params.eventId, {
        Venue : req.body.Venue,
	    number : req.body.number,
	    Organiser : req.body.Organiser,
	    EventName : req.body.EventName,
	    Details : req.body.Details,
	    Time : req.body.Time,
	    MapUrl : req.body.MapUrl,
	    Tags : req.body.Tags,
	    PosterUrl : req.body.PosterUrl,
    }, {new: true})
    .then(event => {
        if(!event) {
            return res.status(404).send({
                message: "Event not found with id " + req.params.eventId
            });
        }
        res.send(event);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Event not found with id " + req.params.eventId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.eventId
        });
    });
};

exports.deleteEventById = (req, res) => {
    Events.findByIdAndRemove(req.params.eventId)
    .then(event => {
        if(!event) {
            return res.status(404).send({
                message: "Event not found with id " + req.params.eventId
            });
        }
        res.send({message: "Event deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.eventId
            });                
        }
        return res.status(500).send({
            message: "Could not delete event with id " + req.params.eventId
        });
    });
};
