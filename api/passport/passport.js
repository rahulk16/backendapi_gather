const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const GooglePlusTokenStrategy = require('passport-google-plus-token');
const LocalStrategy = require('passport-local').Strategy;
const {JWT_SECRET} = require('../configuration');
var Users = require('../models/gatherUser');

//json web token strategy
passport.use( new JwtStrategy({
	jwtFromRequest: ExtractJwt.fromHeader('authorization'),
	secretOrKey: JWT_SECRET
}, async (payload, done) => {
	try{
		//find the user specified in token
		const user = await Users.findById(payload.sub);

		//if user is not there
		if(!user){
			return done(null, false);
		}

		//else return the user
		done(null, user);
	} catch(error){
		done(error, false);
	}
}));

//local strategy
passport.use( new LocalStrategy({
	usernameField: 'email'
}, async (email, password, done) => {

	try{
		//find the user for given email
		const user = await Users.findOne({"local.email" : email});

		//if user is not there
		if(!user){
			return done(null, false);
		}

		//check if password is correct
		const isMatch = await user.isValidPassword(password);

		//incorrect password
		if(!isMatch){
			return done(null,false);
		}

		//otherwise return the user
		done(null, user);
	}catch(error){
		done(error, false);
	}	
}));

// Google OAuth Strategy
passport.use('googleToken', new GooglePlusTokenStrategy({
  clientID: "985510472874-7ocq6h0uhp5efo0c5f4cgjc5iin57tek.apps.googleusercontent.com",
  clientSecret: "gg6t9IvSWs1USwV1NjK6XbUc"
}, async (accessToken, refreshToken, profile, done) => {
  try {
    // Should have full user profile over here
    // console.log('profile', profile);
    // console.log('accessToken', accessToken);
    // console.log('refreshToken', refreshToken);

    const existingUser = await Users.findOne({ "google.id": profile.id });
    if (existingUser) {
      return done(null, existingUser);
    }

    const newUser = new Users({
      method: 'google',
      google: {
        id: profile.id,
        email: profile.emails[0]
      }
    });

    await newUser.save();
    done(null, newUser);
  } catch(error) {
    done(error, false, error.message);
  }
}));
