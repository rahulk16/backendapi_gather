const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;
// var userSchema = new Schema({
// 	email: {
// 		type: String,
// 		required: true,
// 		unique: true,
// 		lowercase: true
// 	},
// 	password: {
// 		type: String,
// 		required: true
// 	}
// });

var userSchema = new Schema({
  method: {
    type: String,
    enum: ['local', 'google', 'facebook'],
    required: true
  },
  local: {
    email: {
      type: String,
      lowercase: true
    },
    password: { 
      type: String
    }
  },
  google: {
    id: {
      type: String
    },
    email: {
      type: String,
      lowercase: true
    }
  },
  facebook: {
    id: {
      type: String
    },
    email: {
      type: String,
      lowercase: true
    }
  }
});

// userSchema.pre('save', async function(next){
// 	try{
// 		//generate a hashed password(salt+hash())
// 		const salt = await bcrypt.genSalt(10);
// 		const hashedPassword = await bcrypt.hash(this.password, salt);
// 		this.password = hashedPassword;
// 		next();

// 	} catch(error){
// 		next(error);
// 	}
// });

userSchema.pre('save', async function(next) {
  try {
    console.log('entered');
    if (this.method !== 'local') {
      next();
    }

    // Generate a salt
    const salt = await bcrypt.genSalt(10);
    // Generate a password hash (salt + hash)
    const passwordHash = await bcrypt.hash(this.local.password, salt);
    // Re-assign hashed version over original, plain text password
    this.local.password = passwordHash;
    console.log('exited');
    next();
  } catch(error) {
    next(error);
  }
});


// userSchema.methods.isValidPassword = async function(newPassword){
// 	try{
// 		return await bcrypt.compare(newPassword, this.password);
// 	} catch(error){
// 		throw new Error(error);
// 	}
// }


userSchema.methods.isValidPassword = async function(newPassword) {
  try {
    return await bcrypt.compare(newPassword, this.local.password);
  } catch(error) {
    throw new Error(error);
  }
}


module.exports = mongoose.model('users',userSchema);