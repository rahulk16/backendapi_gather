module.exports = function(app){
    var searchController = require('../controllers/searchController');
    var userController = require('../controllers/userController');
    const { validateBody, schemas } = require('../helpers/routeHelpers');
    const passport = require('passport');
    const passportConf = require('../passport/passport');

    app.get('/', (req, res) => {
        res.json({"message": "Welcome to GATHER. Have an experience of an intelligent event management system."});});

    app.route('/signup')
    	.post(validateBody(schemas.authSchema), userController.signUp);

    app.route('/signin')
    	.post(validateBody(schemas.authSchema), passport.authenticate('local', {session:false}), userController.signIn);
    
    app.route('/oauth/google')
        .post(passport.authenticate('googleToken', { session: false }), userController.googleOAuth);

    app.route('/search')
        .post(searchController.searchEvents);

    // app.route('/events')
    // 	.post(passport.authenticate('jwt', {session:false}), userController.events)

    app.route('/events')
        .post(userController.creatEvents);

    app.route('/events')
        .get( userController.getAll);

    app.route('/events/:eventId')
        .get( userController.findOneEvent);

    app.route('/events/:eventId')
        .put( userController.updateEventById);

    app.route('/events/:eventId')
        .delete( userController.deleteEventById);
};

