//import packages
const express = require('express');
var bodyParser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');
var Events = require('./api/models/gatherEvent');

//middlewares
var app = express();
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/Gather', {useNewUrlParser:true}).then(_ => console.log('Database Connected...'));

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(morgan('dev'));

//routes
var routes = require('./api/routes/gatherRoutes');
routes(app);

// Events.find({}).then(tagData => {
// 	console.log(Object.values(tagData))
// 	// Algorithmia.client("simz9BDkWduXmr76RbH/f1EZa6c1")
// 	// .algo("nlp/AutoTag/1.0.1?timeout=300") // timeout is optional
// 	// .pipe(tagData.EventName)
// 	// .then(response => {
// 	// 	console.log(response.get());
//  //  });
// });

//start the server 
const port = process.env.PORT || 5050;
app.listen(port, () => {
    console.log(`server is up at port ${port}`);
});
